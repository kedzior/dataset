package structure;

import java.util.List;

public class Problem {
	
	public int fold;
	public String dataset;
	public int iIndex;
	public String sQuestion;
	public List<String> lEquations;
	public List<Double> lSolutions;
	public int templateNumber;

}
